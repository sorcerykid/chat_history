Chat History Mod v3.0 PRERELEASE
By Leslie E. Krause

Chat History is an interactive chat history viewer for use on Minetest servers.

Repository
----------------------

Browse source code...
  https://bitbucket.org/sorcerykid/chat_history

Download archive...
  https://bitbucket.org/sorcerykid/chat_history/get/master.zip
  https://bitbucket.org/sorcerykid/chat_history/get/master.tar.gz

Compatability
----------------------

Minetest 0.4.15+ required

Dependencies
----------------------

ActiveFormspecs Mod v2.6+
  https://bitbucket.org/sorcerykid/formspecs

Chat2 Mod
  https://github.com/minetest-mods/chat2

Configuration Panel Mod
  https://bitbucket.org/sorcerykid/config

Player Registry Mod v2.0+
  https://bitbucket.org/sorcerykid/registry

Installation
----------------------

  1) Unzip the archive into the mods directory of your game
  2) Rename the chat_history-master directory to "chat_history"
  3) Add "chat_history" as a dependency to any mods using the API

Source Code License
----------------------

GNU Lesser General Public License v3 (LGPL-3.0)

Copyright (c) 2017-2020, Leslie E. Krause

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU Lesser General Public License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

http://www.gnu.org/licenses/lgpl-2.1.html

Multimedia License (textures, sounds, and models)
----------------------------------------------------------

Creative Commons Attribution 4.0 International (CC BY 4.0)

	/sounds/mailbox_chime.ogg
	obtained from https://notificationsounds.com/message-tones/oringz-w428-315

	/sounds/mailbox_chime2.ogg
	obtained from https://notificationsounds.com/message-tones/oringz-w432-335

You are free to:
Share — copy and redistribute the material in any medium or format.
Adapt — remix, transform, and build upon the material for any purpose, even commercially.
The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

Attribution — You must give appropriate credit, provide a link to the license, and
indicate if changes were made. You may do so in any reasonable manner, but not in any way
that suggests the licensor endorses you or your use.

No additional restrictions — You may not apply legal terms or technological measures that
legally restrict others from doing anything the license permits.

Notices:

You do not have to comply with the license for elements of the material in the public
domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary
for your intended use. For example, other rights such as publicity, privacy, or moral
rights may limit how you use the material.

For more details:
http://creativecommons.org/licenses/by/4.0/
